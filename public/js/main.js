jQuery(document).ready(function($){

	function getCookie(name) {
	    var value = "; " + document.cookie;
	    var parts = value.split("; " + name + "=");
	    if (parts.length == 2) return parts.pop().split(";").shift();
	}

	var ll = '<div class="wpcsc-page-bottom-alert"><p class="the-wpcsc-warn-text">Some of the content on this page, is shown based on your location ('+getCookie('region')+')  <a href="" id="wpcsc-change-location">Change Location</a></p> <a id="wpcsc-close-warn" href="">X</a></div>';
	$('body').append(ll);
	rerRun();
	function rerRun(){
		$('.wpcsc-page-bottom-alert #wpcsc-close-warn').click(function(e){
			$(this).parent().remove();
			e.preventDefault();
		});
		var pupdata = '<div id="wpcsc-popup-change-country"><div class="inner"><select id="wpcsc-popup-change-country-select">';

		$.each(wpcsc_data.Countries, function(i, e){
			pupdata = pupdata + '<option value="'+i+'">'+e+'</option>';
		});
		pupdata = pupdata +'</select><button id="CloseLocationChangePopup">Reload</button></div></div>';
		$('.wpcsc-page-bottom-alert #wpcsc-change-location').click(function(e){
			$('body').append(pupdata);
			$('#wpcsc-popup-change-country').show();
			e.preventDefault();
			rerRun();
		});

		$('#wpcsc-popup-change-country-select').on('change', function(){
			document.cookie = "country="+$(this).val();
	    	document.cookie = "region="+wpcsc_data.Countries[$(this).val()];
	    	document.cookie = "CountrySetByUser=true";
		});

		$('#CloseLocationChangePopup').click(function(e){
			$('#wpcsc-popup-change-country').hide();
			location.reload();
			e.preventDefault();
		});
	}
	

});