<?php


function wpcsc_shortcode_func( $atts ) {
	//set the country code
	$country = get_option('country_code');


	//check if a country exists with the current cookie
	//if not keep country as the default

	$cqargs = array(
		'post_type' => 'wpcsc_country',
		'post_status' => 'publish'
	);
	$cq = new WP_Query($cqargs);

	foreach ($cq->posts as $ct) {
		if (get_post_meta( $ct->ID, '_wpcsc_country_code', true ) == $_COOKIE['country']) {
			$country = $_COOKIE['country'];
		}
	}

	//get the coresponding country post type


	$args = array(
		'post_type' => 'wpcsc_country',
		'meta_query' => array(
	       	array(
	           'key' => '_wpcsc_country_code',
	           'value' => $country,
	           'compare' => '=',
	       	)
	   	)
	);

	$query = new WP_Query($args);
	$post_idd = $query->posts[0]->ID;
	
	//make the list using UNIID
	$fields = get_option('field_list'); 
	$new_fields = [];

	foreach ($fields as $fi => $fival) {
		$new_fields[$fival['Name']] = $fival['UNIID'];
	}

    $a = shortcode_atts( array('field' => $query->posts[0]->title), $atts );


    if ($new_fields[$a['field']]) {
    	return get_post_meta( $post_idd, '_wpcsc_field_'.$new_fields[$a['field']], true );
    }else{
    	return 'The field name you have entered is not correct, please see <a href="https://wpcsc.devonray.net/documentation">WPCSC documentation</a> for help';
    }
}
add_shortcode( 'wpcsc', 'wpcsc_shortcode_func' );