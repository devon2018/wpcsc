<?php

function wpcsc_country_post_type() {
	register_post_type('wpcsc_country',
		[
			'labels'              => [
				'name'               => __('Countries'),
				'singular_name'      => __('Country'),
				'add_new'            => __('New Country'),
				'add_new_item'       => __('Add New Country'),
				'edit_item'          => __('Edit Country'),
				'new_item'           => __('New Country'),
				'view_item'          => __('View Country'),
				'view_items'         => __('Edit Countries'),
				'search_items'       => __('Search Countries'),
				'not_found'          => __('Country Not Found'),
				'not_found_in_trash' => __('Country Not Found In Trash'),
			],
			'public'              => false,
			'has_archive'         => false,
			'exclude_from_search' => true,
			'show_in_nav_menus'   => true,
			'show_in_menu'        => 'wpcsc',
			'show_ui'             => true,
			'show_in_admin_bar'   => true,
			'supports'            => [
				'title',
			]
		]
	);

}
add_action('init', 'wpcsc_country_post_type');

/**
 * This function adds a meta box with a callback function of my_metabox_callback()
 */
function wpcsc_fields_add_meta_box() {
	$var1 = 'this';
	$var2 = 'that';
	add_meta_box(
		'wpcsc_fields',
		__('Country Fields', 'devonray'),
		'wpcsc_fields_metabox_callback',
		'wpcsc_country',
		'normal',
		'low',
		array()
	);
}
add_action('add_meta_boxes', 'wpcsc_fields_add_meta_box');

function wpcsc_fields_metabox_callback($post, $metabox) {
	// Output last time the post was modified.
	global $content_width, $_wp_additional_image_sizes;
	wp_nonce_field('wpcsc_fields_metabox', 'wpcsc_fields_metabox_nonce');
	?>
	<table class="form-table">
	      <tr valign="top">
	        <th scope="row"><label for="country">Choose a country *</label></th>
	        <td>
	          <select name="wpcsc_country_code" id="country" required="" class="wpcsc_100">
	              <option selected disabled>-- Choose a Default Country --</option>
	<?php
	$items = getAvailableCountries();
	asort($items);
	foreach ($items as $code => $name) {
		if ($code == get_post_meta($post->ID, '_wpcsc_country_code', true)) {
			echo "<option value=".$code." selected>".$name."</option>";
		} else {
			echo "<option value=".$code.">".$name."</option>";
		}
	}?>
	</select>
	        </td>
	      </tr>
	    </table>
	<?php
	$theFields = get_option('field_list');

	// wpcsc_field_UNIID
	foreach ($theFields as $field) {
		switch ($field['Type']) {
			case 'text':?>
				                      <table class="form-table">
			                        <tr valign="top">
			                          <th scope="row"><label for="wpcsc_field_	<?php echo $field['UNIID'];?>"><?php echo $field['Name'];
				?>: </label></th>
			                          <td>
			                            <input type="text" name="wpcsc_field_	<?php echo $field['UNIID'];?>" value="<?php echo get_post_meta($post->ID, '_wpcsc_field_'.$field['UNIID'], true);?>" class="wpcsc_100" />
			                          </td>
			                        </tr>
			                      </table>
				<?php break;
			case 'textarea':?>
				                     <table class="form-table">
			                      <tr valign="top">
			                        <th scope="row"><label for="wpcsc_field_	<?php echo $field['UNIID'];?>"><?php echo $field['Name'];
				?>:</label></th>
			                        <td>
			                          <textarea name="wpcsc_field_	<?php echo $field['UNIID'];?>" rows="6" style="width: 100%;"><?php echo get_post_meta($post->ID, '_wpcsc_field_'.$field['UNIID'], true);
				?></textarea>
			                        </td>
			                      </tr>
			                    </table>
				<?php break;
			case 'file':
				$file_id           = get_post_meta($post->ID, '_wpcsc_field_'.$field['UNIID'], true);
				$old_content_width = $content_width;
				$content_width     = 254;?>
				                  <table class="form-table">
			                      <tr valign="top">
			                        <th scope="row"><label for="wpcsc_field_	<?php echo $field['UNIID'];?>"><?php echo $field['Name'];
				?>:</label></th>
			                        <td>
				<?php if ($file_id && get_post($file_id)) {
					//if a file is already set
					//show file name and change file link
					$attachment_title = get_the_title($file_id);

					?>
					                          <p class="hide-if-no-js"><span class="file-title-to-hide-on-delete"><?php echo $attachment_title;
					?></span><a href="javascript:;" id="remove_listing_image_button" >( Remove Attachment)</a></p>

					<?php
				} else {

					$content = '<p class="hide-if-no-js"><span class="file-title-show-on-upload">
                      File Title
                    </span><a title="'	.esc_attr__('Choose an Attachment', 'text-domain').'" href="javascript:;" id="upload_listing_image_button" id="set-listing-image" data-uploader_title="'.esc_attr__('Choose an Attachment', 'text-domain').'" data-uploader_button_text="'.esc_attr__('Choose an Attachment', 'text-domain').'">'.esc_html__('Choose an Attachment', 'text-domain').'</a></p>';

					$content .= '<input type="hidden" id="upload_listing_image" name="wpcsc_field_'.$field['UNIID'].'" value="" />';

				}
				echo $content;

				?>
				</td>
			                      </tr>
			                    </table>
				<?php break;
			default:?>
				<p>Some THing Else</p>
				<?php break;
		}
	}

}

function wpcsc_fields_save_metabox($post_id) {
	// Check if our nonce is set.
	if (!isset($_POST['wpcsc_fields_metabox_nonce'])) {
		return;
	}
	$nonce = $_POST['wpcsc_fields_metabox_nonce'];
	// Verify that the nonce is valid.
	if (!wp_verify_nonce($nonce, 'wpcsc_fields_metabox')) {
		return;
	}
	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
		return;
	}
	// Check the user's permissions.
	if (!current_user_can('edit_post', $post_id)) {
		return;
	}

	$new_fields = array(
		'_wpcsc_country_code' => $_POST['wpcsc_country_code'],
	);

	$theFields = get_option('field_list');

	foreach ($theFields as $field) {
		$new_fields['_wpcsc_field_'.$field['UNIID']] = $_POST['wpcsc_field_'.$field['UNIID']];
	}

	$somethingChanged = false;

	foreach ($new_fields as $new_field => $new_field_key) {
		if (isset($new_field_key)) {
			$somethingChanged = true;
			break;
		}
	}

	if (!$somethingChanged) {
		return;
	}

	foreach ($new_fields as $new_field => $new_field_key) {
		if (empty($new_field_key)) {
			delete_post_meta($post_id, $new_field);
		} else {
			update_post_meta($post_id, $new_field, $new_field_key);
		}
	}

	// Update the meta fields in the database, or clean up after ourselves.

}
add_action('save_post', 'wpcsc_fields_save_metabox');

function wpcsc_register_wpcsc_menu_item() {
	add_menu_page(
		__('WPCSC', 'devonray'),
		'WPCSC',
		'administrator',
		'wpcsc',
		'my_cool_plugin_settings_page',
		'dashicons-admin-settings',
		75
	);
}
add_action('admin_menu', 'wpcsc_register_wpcsc_menu_item');

function wpdocs_register_my_custom_submenu_page() {
	$page = add_submenu_page(
		'wpcsc',
		'Settings',
		'Settings',
		'administrator',
		'wpcsc-settings',
		'wpcsc_settings_page');

	add_action('admin_print_scripts-'.$page, 'my_plugin_admin_scripts');
}

add_action('admin_menu', 'wpdocs_register_my_custom_submenu_page');

?>
