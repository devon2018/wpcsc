jQuery(document).ready(function($){
	$('.removeMyParentAndMe').click(function(e){
		$(this).parent().remove();
		e.preventDefault();
	});

	$('#AddSingleFieldButton').click(function(e){
		$num = $('.singleField').length + 1;
		$unique_id = Math.floor(Math.random() * 9999999);
		$dt = '<div class="singleField"><input type="text" name="field_list['+$num+'][Name]" id="singleFieldName" placeholder="Field Name"><select name="field_list['+$num+'][Type]"><option value="text">Text</option><option value="textarea">Text Area</option><option value="file">File</option></select><input type="hidden" name="field_list['+$num+'][UNIID]" value="'+$unique_id+'"><button class="removeMyParentAndMe">Remove Field</button></div>';
		$('.CountrySpecificFields').append($dt);
		$('#SaveChangesWarning').show();
		e.preventDefault();
	});
		// Uploading files
	var file_frame;

	jQuery.fn.upload_listing_image = function( button ) {
		var button_id = button.attr('id');
		var field_id = button_id.replace( '_button', '' );
		

		// If the media frame already exists, reopen it.
		if ( file_frame ) {
		  file_frame.open();
		  return;
		}
		
		// Create the media frame.
		file_frame = wp.media.frames.file_frame = wp.media({
		  title: jQuery( this ).data( 'uploader_title' ),
		  button: {
		    text: jQuery( this ).data( 'uploader_button_text' ),
		  },
		  multiple: false
		});
		alert('works');
		
		// When an image is selected, run a callback.
		file_frame.on( 'select', function() {
		  var attachment = file_frame.state().get('selection').first().toJSON();
		  jQuery("#"+field_id).val(attachment.id);
		  jQuery("#wpcsc_fields img").attr('src',attachment.url);
		  jQuery( '#wpcsc_fields img' ).show();
		  jQuery( '#' + button_id ).attr( 'id', 'remove_listing_image_button' );
		  jQuery( '#remove_listing_image_button' ).text( 'Remove listing image' );
		});

		// Finally, open the modal
		file_frame.open();
	};

	jQuery('#wpcsc_fields').on( 'click', '#upload_listing_image_button', function( event ) {
		event.preventDefault();

		jQuery.fn.upload_listing_image( jQuery(this) );
	});

	jQuery('#wpcsc_fields').on( 'click', '#remove_listing_image_button', function( event ) {
		event.preventDefault();
		jQuery(this).parent().find('.file-title-to-hide-on-delete').remove();
		jQuery( '#upload_listing_image' ).val( '' );
		jQuery( '#wpcsc_fields img' ).attr( 'src', '' );
		jQuery( '#wpcsc_fields img' ).hide();
		jQuery( this ).attr( 'id', 'upload_listing_image_button' );
		jQuery( '#upload_listing_image_button' ).text( 'Set listing image' );
	});

});