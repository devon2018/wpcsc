<?php
/*
Plugin Name: WordPress Country Specific Content
Plugin URI: https://devonray.net/wpcsc
Description: Premium WordPress Plugin for displaying Country Specific Content
Version: 1.0
Author: Devon Ray
Author URI: https://devonray.net
License:      GPL2

WPCSC is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

WPCSC is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with WPCSC. If not, see {URI to Plugin License}.
 */

defined('ABSPATH') OR exit;

function wpcsc_initialise() {

	if (!current_user_can('activate_plugins')) {
		return;
	}

	$plugin = isset($_REQUEST['plugin'])?$_REQUEST['plugin']:'';
	check_admin_referer("activate-plugin_{$plugin}");

	//create country post types
	wpcsc_country_post_type();
}
register_activation_hook(__FILE__, 'wpcsc_initialise');

require_once ('includes/wpcsc-post-types.php');
require_once ('includes/available-countries.php');

add_action('admin_init', 'my_plugin_admin_init');
function my_plugin_admin_init() {

	wp_enqueue_style('wpcsc_backend_styles', plugin_dir_url(__FILE__).'admin/css/main.css');
	wp_register_script('wpcsc_backend_script', plugin_dir_url(__FILE__).'admin/js/main.js', array('jquery'));
	wp_enqueue_script('wpcsc_backend_script');
	wp_enqueue_media();
}

function my_plugin_admin_scripts() {
	/*
	 * It will be called only on your plugin admin page, enqueue our script here
	 */
	wp_enqueue_script('wpcsc_backend_script');
}

function wpcsc_frontend_scripts() {
	wp_enqueue_style('wpcsc_frontend_styles', plugin_dir_url(__FILE__).'public/css/main.css');
	wp_enqueue_script('wpcsc_frontend', plugin_dir_url(__FILE__).'/public/js/main.js', array('jquery'));
	wp_localize_script('wpcsc_frontend', 'wpcsc_data', array('Countries' => getAvailableCountries()));
}
add_action('wp_enqueue_scripts', 'wpcsc_frontend_scripts');

function setCookiesRight() {
	$xml = \simplexml_load_file("http://www.geoplugin.net/xml.gp?ip=".getRealIpAddr());

	//die(var_dump($xml));

	if ($_COOKIE['CountrySetByUser'] != 'true') {
		setcookie('country', $xml->geoplugin_countryCode);
		setcookie('region', $xml->geoplugin_countryName);
	}
}

add_action('wp', 'setCookiesRight');

//Menu Settings and Post Types
function getRealIpAddr() {
	if (!empty($_SERVER['HTTP_CLIENT_IP']))//check ip from share internet
	{
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))//to check ip is pass from proxy
	{
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$ip = $_SERVER['REMOTE_ADDR'];
	}

	return $ip;
}

require_once ('includes/shortcode-settings.php');

if (is_admin()) {// admin actions
	add_action('admin_init', 'register_wpcsc_settings');
} else {
	// non-admin enqueues, actions, and filters
}

function register_wpcsc_settings() {// whitelist options
	register_setting('wpcsc-settings-group', 'country_code');
	register_setting('wpcsc-settings-group', 'field_list');
}

function wpcsc_settings_page() {
	?>
	<div class="wrap">
						<h1>WordPress Country Specific Content Settings</h1></div><form method="post" action="options.php" class="wpcsc_settings_form">
	<?php settings_fields('wpcsc-settings-group');
	do_settings_sections('wpcsc-settings-group');
	?>
	<table class="form-table"><tr valign="top"><th scope="row"><label for="ChooseADefaultCountry">Choose a default Country To show, if no data is specificaly set</label></th>
					<td><select name="country_code" id="ChooseADefaultCountry">
					<option selected disabled>-- Choose a Default Country --</option>
	<?php
	$items = getAvailableCountries();
	asort($items);
	foreach ($items as $code => $name) {
		if ($code == get_option('country_code')) {
			echo "<option value=".$code." selected>".$name."</option>";
		} else {
			echo "<option value=".$code.">".$name."</option>";
		}

	}?>
	</select></td>
											</tr>
					</table>
					<div class="CountrySpecificFields">
						<p>Create Fields That you would like to display differently for each country.<br><strong>Field Name Should be one word all lowercase</strong></p>
	<?php

	$fields = get_option('field_list');

	$count = 0;?>


	<?php foreach ($fields as $values):?>

							<div class="singleField">
								<input type="text" name="field_list[<?php echo $count;?>][Name]" id="singleFieldName" placeholder="Field Name" value="<?php echo $values['Name'];?>">
								<select name="field_list[<?php echo $count;?>][Type]">
								<option value="text" <?php if ($values['Type'] == 'text'):?>selected<?php endif?>>Text</option>
								<option value="textarea" <?php if ($values['Type'] == 'textarea'):?>selected<?php endif?>>Text Area</option>
								<option value="file" <?php if ($values['Type'] == 'file'):?>selected<?php endif?>>File</option>
							</select>
						<input type="hidden" name="field_list[<?php echo $count;?>][UNIID]" value="<?php echo $values['UNIID'];?>">
						<button class="removeMyParentAndMe">Remove Field</button>
						<span class="shortcode">[wpcsc field="<?php echo $values['Name'];?>"]</span>
					</div>

	<?php $count = $count+1;endforeach?>
	</div>
						<button id="AddSingleFieldButton">Add Field</button>
					<br>

	<?php submit_button();?>
	<p id="SaveChangesWarning">Save changes to generate shortcodes</p>
					</form>
					</div>

	<?php }

//name
//type enum(text, textarea, image)

function wpcsc_plugin_deactivate() {
	if (!current_user_can('activate_plugins')) {
		return;
	}

	$plugin = isset($_REQUEST['plugin'])?$_REQUEST['plugin']:'';
	check_admin_referer("deactivate-plugin_{$plugin}");

	unregister_post_type('wpcsc_country');
	flush_rewrite_rules();
}

register_deactivation_hook(__FILE__, 'wpcsc_plugin_deactivate');

function wpcsc_plugin_uninstall() {
	if (!current_user_can('activate_plugins')) {
		return;
	}

	//check_admin_referer('bulk-plugins');

	// Important: Check if the file is the one
	// that was registered during the uninstall hook.
	if (!defined('WP_UNINSTALL_PLUGIN')) {
		die;
	}

	//unregister_post_type('wpcsc_country');
	//flush_rewrite_rules();
	delete_option('country_code');
	delete_option('field_list');

}
register_uninstall_hook(__FILE__, 'wpcsc_plugin_uninstall');
